# EnumGenerator

#### 项目介绍
idea 插件
初始化java enum 对象 并且生成属性对应的valueOf方法



#### 安装教程


#### 使用说明
1. 在枚举类中点击generator菜单（windows alt+insert），点击EnumGenerator即可
![avatar](./example/step1.png)
![avatar](./example/step2.png)

#### 参与贡献

1. 暂无


#### 码云特技

1. 其他idea插件 [idea-mybatis-generator](https://gitee.com/rohou/mybatis-generator)